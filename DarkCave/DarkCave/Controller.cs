﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DarkCave
{
    class Controller
    {
        private const Keys moveRight = Keys.Right;
        private const Keys moveLeft = Keys.Left;
        private const Keys jump = Keys.X;
        private const Keys crouch = Keys.Down;
        private const Keys attack = Keys.Z;
        private const Keys enter = Keys.Enter;

        private bool keyPressed;

        public void ReadInput(KeyboardState ks, Player player, GameTime gameTime)
        {
	        switch (GameState.State)
	        {
		        case GameStates.GameStart:
			        if (!PlayerState.Dead)
			        {
                        if (!PlayerState.MustCrouch)
                        {
                            if ((!(ks.IsKeyDown(moveRight) || ks.IsKeyDown(moveLeft) || ks.IsKeyDown(crouch))) ||
                                (ks.IsKeyDown(moveRight) && ks.IsKeyDown(moveLeft)))
                            {
                                player.Stand();
                                if (PlayerState.Crouched)
                                    PlayerState.Crouched = false;
                            }
                            else if (ks.IsKeyDown(moveRight) && ks.IsKeyDown(moveLeft) && ks.IsKeyDown(crouch))
                            {
                                player.Crouch();
                                if (!PlayerState.Crouched)
                                    PlayerState.Crouched = true;
                            }
                            else if (ks.IsKeyDown(moveRight) && ks.IsKeyDown(crouch))
                            {
                                player.Move(1);
                                if (!PlayerState.Crouched)
                                    PlayerState.Crouched = true;
                            }
                            else if (ks.IsKeyDown(moveLeft) && ks.IsKeyDown(crouch))
                            {
                                player.Move(-1);
                                if (!PlayerState.Crouched)
                                    PlayerState.Crouched = true;
                            }
                            else if (ks.IsKeyDown(moveRight))
                            {
                                player.Move(1);
                                if (PlayerState.Crouched)
                                    PlayerState.Crouched = false;
                            }
                            else if (ks.IsKeyDown(moveLeft))
                            {
                                player.Move(-1);
                                if (PlayerState.Crouched)
                                    PlayerState.Crouched = false;
                            }
                            else if (ks.IsKeyDown(crouch))
                            {
                                player.Crouch();
                                if (!PlayerState.Crouched)
                                    PlayerState.Crouched = true;
                            }

                            if (ks.IsKeyDown(jump) && !PlayerState.Crouched && !PlayerState.MustCrouch)
                            {
                                player.Jump();
                            }

                            if (ks.IsKeyDown(attack) && !PlayerState.Crouched && !PlayerState.MustCrouch)
                            {
                                player.Attack();
                            }
                        }
                        else
                        {
                            player.Crouch();
                            PlayerState.Crouched = true;
                            if (!(ks.IsKeyDown(moveRight) && ks.IsKeyDown(moveLeft)))
                            {
                                if (ks.IsKeyDown(moveRight))
                                {
                                    player.Move(1);
                                }
                                else if (ks.IsKeyDown(moveLeft))
                                {
                                    player.Move(-1);
                                }
                                else
                                {
                                    PlayerState.MustCrouch = false;
                                    player.Crouch();
                                }
                            }
                            else
                            {
                                PlayerState.MustCrouch = false;
                                player.Crouch();
                            }
                            PlayerState.MustCrouch = false;
                        }
			        }
                    if ((PlayerState.Dead && ks.IsKeyDown(enter)) || (ks.IsKeyDown(Keys.RightControl) && ks.IsKeyDown(enter)))
			        {
				        player.Respawn();
			        }
			        if (ks.IsKeyDown(Keys.RightControl) && ks.IsKeyDown(Keys.M))
			        {
				        if (!keyPressed)
				        {
					        Camera.BlackEnabled = !Camera.BlackEnabled; 
				        }
				        keyPressed = true;
			        }
			        else
			        {
				        keyPressed = false;
			        }
			        break;
		        case GameStates.TitleScreen:
			        if (ks.IsKeyDown(enter))
			        {
				        GameState.State = GameStates.GameStart;
			        }
			        break;
		        case GameStates.GameEnd:
			        if (ks.IsKeyDown(enter))
			        {
				        Environment.Exit(0);
			        }
			        break;
				case GameStates.LevelEnd:
			        if (ks.IsKeyDown(enter))
			        {
				        GameState.State = GameStates.LevelChange;
			        }
					break;
	        }
        }
    }
}
