﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace DarkCave
{
    class Tile
    {
        static Texture2D TILESHEET;
        public static int TILESIZE;
        static int SHEETSIZE;

        public Vector2 Position;
        public int Type;

        public Tile(int Type, int x, int y)
        {
            Position = new Vector2(x, y);
            this.Type = Type;
        }

        public static void setSpritesheet(Texture2D SpriteSheet, int TileSize)
        {
            TILESHEET = SpriteSheet;
            TILESIZE = TileSize;
            SHEETSIZE = TILESHEET.Width/TILESIZE;
        }

        public void Draw(SpriteBatch SpriteBatch,Vector2 Camera)
        {
            SpriteBatch.Draw(TILESHEET, Position * TILESIZE + Camera,new Rectangle((Type % SHEETSIZE) * TILESIZE, ((int)Math.Floor((double)Type / (double)SHEETSIZE) * TILESIZE), TILESIZE, TILESIZE), Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0);
        }
    }
}
