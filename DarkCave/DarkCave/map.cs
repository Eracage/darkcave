﻿using System.Collections.Generic;
using System.IO;

namespace DarkCave
{
	class Map
	{
		public List<Tile> Tiles = new List<Tile>();

		private List<string> map = new List<string>();
        private List<int> width = new List<int>();
		public Map(string path)
		{
			using (StreamReader reader = new StreamReader(path))
			{
				string line = reader.ReadLine();

				while (line != null)
				{
                    width.Add(line.Length);
					map.Add(line);
					line = reader.ReadLine();
				}
			}
		}

		public void CreateTypeDefinations()
		{
			for (int y = 0; y < map.Count; y++)
			{
				for (int x = 0; x < width[y]; x++)
                {
                    int type = map[y][x];

                    //Conversion from ASCII chars 0-9, a-z, A-Z in map file to numbers 0-61
                    if (47 < type && type < 58)
                    {
                        type -= 48;
                    }
                    else if (96 < type && type < 123)
                    {
                        type -= 87;
                    }
                    else if (64 < type && type < 91)
                    {
                        type -= 29;
                    }

					Tiles.Add(new Tile(type, x, y));
				}
			}
		}
	}
}