﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;

namespace DarkCave
{
    class TitleSpark
    {
        private readonly Texture2D sparkTexture;
        private Vector2 position;
        private Vector2 direction;
        private readonly float speed;
        private readonly float fireRandom;
        private float colour;
        private readonly float sColour;
        private float r, g, b, a;
        private float multiplier;

        public float Life
        {
            get { return colour; }
        }

        public TitleSpark(Texture2D SparkTexture, Vector2 Position, float FireRandom, Vector2 Direction, float Multiplier)
        {
            a = 255f;
            sparkTexture = SparkTexture;
            position = Position;
            fireRandom = FireRandom;
            multiplier = Multiplier;

            colour = PlayerState.Random.Next(0, 1400) / (float)PlayerState.Random.Next((int)(90/(3+multiplier)), (int)(100/(3+multiplier)));
            sColour = colour;

            direction = 0.33f * Direction + new Vector2((PlayerState.Random.Next(0, (int)(151 + 50 * multiplier)) - (int)(75 + 25 * multiplier) + fireRandom + 0.5f * fireRandom * multiplier) / (PlayerState.Random.Next((int)(100/multiplier), (int)(240/multiplier))), ((PlayerState.Random.Next(0, 81) - 38f ) / (float)(Math.Round(Math.Abs(0.001f*fireRandom))+1)) / -(PlayerState.Random.Next(20, 31)));
            speed = (PlayerState.Random.Next(90, 111)) / 600f;
        }

        public void Update()
        {
            direction.X *= (float)Math.Pow(0.998f,multiplier);
            direction.Y = (direction.Y + 5+ 2*multiplier) * 0.98f - 5 - 2*multiplier;
            position += new Vector2(direction.X * speed, (direction.Y) * speed);
            colour -= 1;
            r = 6 * colour * colour / (sColour * sColour);
            g = 1.3f * colour * colour / (sColour * sColour);
            b = 0.5f * colour * colour / (sColour * sColour);
            //a = (float)(1 - (Math.Tanh((5 * sColour - 10 * colour) / (sColour)))) / 2f;
            a = (float)Math.Pow(((1 + (Math.Sin((Math.PI * ((colour / (sColour*2))-1/2)))) / 2f)), 1);
            //a = (float)Math.Pow(a, -colour);
            //a = (2.2f * colour + 0.25f * (colour/sColour)) / sColour;
        }

        public void Draw(SpriteBatch SpriteBatch)
        {
            SpriteBatch.Draw(sparkTexture, position, null, new Color(r, g, b, a), 0, Vector2.Zero, (0.5f * multiplier), SpriteEffects.None, 0.7908f + (colour / sColour) / 100f);
        }
    }
}