﻿using Microsoft.Xna.Framework;

namespace DarkCave
{
    static class Camera
    {
        public static Vector2 Position = Vector2.Zero;
        public static bool BlackEnabled = true;
    }
}
