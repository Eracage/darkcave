﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DarkCave
{
    class Spark
    {
        private readonly Texture2D sparkTexture;
        private Vector2 position;
        private Vector2 direction;
        private readonly float speed;
        private readonly float fireRandom;
        private float colour;
        private readonly float sColour;
        private float r, g, b, a;

        public float Life
        {
            get { return colour; }
        }

        public Spark(Texture2D SparkTexture, Vector2 Position, float FireRandom, Vector2 Direction)
        {
            a = 255f;
            sparkTexture = SparkTexture;
            position = Position;
            fireRandom = FireRandom;

            colour = PlayerState.Random.Next(0, 800) / (float)PlayerState.Random.Next(20, 80);
            sColour = colour;

            direction = 0.33f * Direction + new Vector2((PlayerState.Random.Next(0, 121) - 60 + 1.2f *fireRandom) / (PlayerState.Random.Next(35, 151)), (PlayerState.Random.Next(0, 81) - 20f) / -(PlayerState.Random.Next(20, 51)));
            speed = (PlayerState.Random.Next(90, 111)) / 600f;
        }

        public void Update()
        {
            direction.X *= 0.97f;
            direction.Y = (direction.Y + 10) * 0.99f - 10;
            position += new Vector2(direction.X * speed, (direction.Y) * speed);
            colour -= 1;
            r = 3 * colour * colour / (sColour * sColour);
            g = 1 * colour * colour / (sColour * sColour);
            b = 0.35f * colour * colour / (sColour * sColour);
            a = (float)(1 - (Math.Tanh((5 * sColour - 10 * colour) / (sColour)))) / 2f;
            //a = (float)Math.Pow(((1 + (Math.Sin((Math.PI * colour * 0.3f) / sColour))) / 2f),2.5f);
            //a = (float)Math.Pow(a, -colour);
            //a = (2.2f * colour + 0.25f * (colour/sColour)) / sColour;
        }

        public void Draw(SpriteBatch SpriteBatch)
        {
            SpriteBatch.Draw(sparkTexture, position + Camera.Position, null, new Color(r, g, b, a), 0, Vector2.Zero, 0.5f, SpriteEffects.None, 0.7908f + (colour / sColour) / 100f);
        }
    }
}
