﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace DarkCave
{
    class HitboxController
    {
        private readonly Player player;
        private readonly List<Hitbox> mapHitboxes = new List<Hitbox>();
        private List<Tile> tiles;
        private const int onTileFix = 12;
        private const int getOnTileHeight = 22;
        private bool wasCrouched = false;
        private bool wasAirborne = false;

        public List<Tile> MapTiles
        {
            set
            {
                mapHitboxes.Clear();

                tiles = value;

                for (int i = 0; i < tiles.Count(); i++)
                {
                    mapHitboxes.Add(new Hitbox(tiles[i].Position * Tile.TILESIZE, Tile.TILESIZE, Tile.TILESIZE, tiles[i].Type, Vector2.Zero));
                }
            }
        }

        public HitboxController(Player Player, List<Tile> MapTiles)
        {
            player = Player;

            tiles = MapTiles;

            for (int i = 0; i < tiles.Count(); i++)
            {
                mapHitboxes.Add(new Hitbox(tiles[i].Position * Tile.TILESIZE, Tile.TILESIZE, Tile.TILESIZE, tiles[i].Type, Vector2.Zero));
            }
        }

        public void Update()
        {
            wasCrouched = PlayerState.Crouched;
            //PlayerState.MustCrouch = false;

            wasAirborne = PlayerState.Airborne;
            if (!PlayerState.Airborne)
                PlayerState.Airborne = true;

            for (int i = 0; i < tiles.Count(); i++)
            {
                if (player.PlayerHitbox.Box.Intersects(mapHitboxes[i].Box))
                {
                    if (mapHitboxes[i].Type > 0)
                    {
                        xCorrection(i);

                        if (player.PlayerHitbox.Box.Intersects(mapHitboxes[i].Box))
                        {
                           yCorrection(i); 
                        }

                    }

                    if (player.PlayerHitbox.Box.Intersects(mapHitboxes[i].Box) && mapHitboxes[i].Type > 20)
                    {
                        if (GameState.State != GameStates.LevelEnd)
                        {
                            DarkCave.CurrentMap++;
                        }
                        GameState.State = GameStates.LevelEnd;
                    }
                }
            }
        }

        private void xCorrection(int i)
        {
            if (!wasAirborne && wasCrouched)
            {
                if (player.Position.Y> mapHitboxes[i].Position.Y + getOnTileHeight &&
                    player.Position.Y< mapHitboxes[i].Position.Y + mapHitboxes[i].Height + getOnTileHeight)
                {
                    player.Position.X -= player.Velocity.X;
                    //player.Velocity.X = 0;
                    player.PlayerHitbox.Update(player.Position);
                }
            }
            else 
            {
                if (player.Position.Y + player.Velocity.Y > mapHitboxes[i].Position.Y + getOnTileHeight)
                {
                    player.Position.X -= player.Velocity.X;
                    //player.Velocity.X = 0;
                    player.PlayerHitbox.Update(player.Position);
                }
            }
        }

        private void yCorrection(int i)
        {
            if (!wasCrouched)
            {
                if (player.Position.Y - player.Velocity.Y >
                    mapHitboxes[i].Position.Y + mapHitboxes[i].Height + player.PlayerHitbox.Height)
                {
                    player.Position.Y = mapHitboxes[i].Position.Y + mapHitboxes[i].Height + player.PlayerHitbox.Height+1;
                    player.Velocity.Y = 0;
                }
            }
            else
            {
                if (player.Position.Y - player.Velocity.Y >
                    mapHitboxes[i].Position.Y + mapHitboxes[i].Height + getOnTileHeight)
                {
                    player.MustStayCrouched();
                    player.Velocity.Y = 0;
                }
            }

            if (player.Velocity.Y>=0)
            {
                if (player.Position.Y - player.Velocity.Y < mapHitboxes[i].Position.Y + getOnTileHeight)
                {
                    player.Position.Y = mapHitboxes[i].Position.Y + onTileFix;
                    player.Velocity.Y = 0;
                    PlayerState.Airborne = false;
                } 
            }
        }
    }
}
