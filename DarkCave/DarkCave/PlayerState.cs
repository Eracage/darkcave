﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;

namespace DarkCave
{
    static class PlayerState
    {
        public static bool Attacking = false;
        public static bool Airborne = true;
        public static bool Dead = false;
        public static bool Crouched = false;
        public static bool MustCrouch = false;
        public static Random Random = new Random();
    }
}
