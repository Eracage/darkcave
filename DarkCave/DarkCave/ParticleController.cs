﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DarkCave
{
    class ParticleController
    {
        private readonly List<Spark> fire;
        private readonly List<TitleSpark> titlefire;
        private readonly Texture2D sparkTexture;
        private float fireRandomizer;
        private int fireCounter;
        private float flameDirection;
        private Vector2 prevLightPosition;
        private Vector2 direction;

        private readonly List<Vector2> fireExtender = new List<Vector2> 
        {  new Vector2(-1, -1), new Vector2(0, -1) ,new Vector2(1, -1), 
            new Vector2(-2, 0), new Vector2(-1, 0), new Vector2(0, 0), new Vector2(1, 0), new Vector2(2, 0), 
            new Vector2(-2, 1), new Vector2(-1, 1), new Vector2(0, 1), new Vector2(1, 1), new Vector2(2, 1), 
            new Vector2(-2, 2), new Vector2(-1, 2), new Vector2(0, 2), new Vector2(1, 2), new Vector2(2, 2), 
            new Vector2(-3, 3), new Vector2(-2, 3), new Vector2(-1, 3), new Vector2(0, 3), new Vector2(1, 3), new Vector2(2, 3), new Vector2(3, 3),
            new Vector2(-3, 4), new Vector2(-2, 4), new Vector2(-1, 4), new Vector2(0, 4), new Vector2(1, 4), new Vector2(2, 4), new Vector2(3, 4),
            new Vector2(-3, 5), new Vector2(-2, 5), new Vector2(-1, 5), new Vector2(0, 5), new Vector2(1, 5), new Vector2(2, 5), new Vector2(3, 5),
            new Vector2(-3, 6), new Vector2(-2, 6), new Vector2(-1, 6), new Vector2(0, 6), new Vector2(1, 6), new Vector2(2, 6), new Vector2(3, 6),};

        private readonly List<Vector2> titleScreenFireExtender = new List<Vector2> 
        {   
                                                     new Vector2(-1, 1),
                               new Vector2(-2, 2),                         new Vector2(0, 2),
                                                     new Vector2(-1, 3),                        new Vector2(0, 3),
         new Vector2(-2.5f, 4),new Vector2(-2, 4),                         new Vector2(0, 4),                        new Vector2(1.3f, 4),
            new Vector2(-3, 5),                      new Vector2(-1, 5),                        new Vector2(1, 5),   new Vector2(2f, 5),
                               new Vector2(-2, 6),                         new Vector2(0, 6),                        new Vector2(1.5f, 6),new Vector2(2.5f, 6),
            new Vector2(-3, 7),                      new Vector2(-1, 7),                        new Vector2(0.5f, 7),                     new Vector2(3, 7),
         new Vector2(-2.5f, 8),new Vector2(-2, 8),                                              new Vector2(1, 8.4f),new Vector2(2, 8.3f),new Vector2(3, 8.5f),
                               new Vector2(-2, 8.7f),new Vector2(-1, 8.7f),new Vector2(0, 8.5f),

        };

        private readonly List<Vector2> endScreenFireExtender = new List<Vector2> 
        {  
                                                                              new Vector2(0,-2),                               
                                                        new Vector2(-2,2),    new Vector2(0,1),    new Vector2(2,2),                          
                                  new Vector2(-4,4),    new Vector2(-2,5),    new Vector2(0,4),    new Vector2(2,5),    new Vector2(4,4),  
                                  new Vector2(-4,6),    new Vector2(-2,7),    new Vector2(0,6),    new Vector2(2,7),    new Vector2(4,6),                          
                                  new Vector2(-4,8),    new Vector2(-2,9),    new Vector2(0,8),    new Vector2(2,9),    new Vector2(4,8),                          
            new Vector2(-5.6f,11),new Vector2(-4,10),   new Vector2(-2,11),   new Vector2(0,10),   new Vector2(2,11),   new Vector2(4,10),   new Vector2(5.6f,11),
            new Vector2(-5.8f,13),new Vector2(-4,12),   new Vector2(-2,13),   new Vector2(0,12),   new Vector2(2,13),   new Vector2(4,12),   new Vector2(5.8f,13),
            new Vector2(-6,15),   new Vector2(-4,14),   new Vector2(-2,15),   new Vector2(0,14),   new Vector2(2,15),   new Vector2(4,14),   new Vector2(6,15),
            new Vector2(-6,17),   new Vector2(-4,16),   new Vector2(-2,17),   new Vector2(0,16),   new Vector2(2,17),   new Vector2(4,16),   new Vector2(6,17),
            new Vector2(-5.5f,19),new Vector2(-4,18),   new Vector2(-2,19),   new Vector2(0,18),   new Vector2(2,19),   new Vector2(4,18),   new Vector2(6,19),
            new Vector2(-5.2f,21),new Vector2(-4,20),   new Vector2(-2,21),   new Vector2(0,20),   new Vector2(2,21),   new Vector2(4,20),   new Vector2(6,21),
                                  new Vector2(-4,21),   new Vector2(-2,22),   new Vector2(0,21.5f),new Vector2(2,22),   new Vector2(4,21),   new Vector2(6,21),
                                  new Vector2(-4,22),   new Vector2(-2,23),   new Vector2(0,23),   new Vector2(2,22.6f),new Vector2(4,22),
                                                   

        };

        public ParticleController(Texture2D SparkTexture)
        {
            sparkTexture = SparkTexture;
            fire = new List<Spark>();
            titlefire = new List<TitleSpark>();
        }

        public void Update(Vector2 LightPosition)
        {
            if (GameState.State == GameStates.GameStart)
            {
                Add(LightPosition);
                titlefire.Clear();
            }
            else if (GameState.State == GameStates.TitleScreen)
            {
                TitleAdd(new Vector2(808,246),2);
            }
            else if (GameState.State == GameStates.GameEnd)
            {
                fire.Clear();
                TitleAdd(new Vector2(975,290),5);
            }


            for (int i = fire.Count - 1; i >= 0; i--)
            {
                fire[i].Update();

                if (fire[i].Life <= 0)
                {
                    fire.RemoveAt(i);
                }
            }

            for (int i = titlefire.Count - 1; i >= 0; i--)
            {
                titlefire[i].Update();

                if (titlefire[i].Life <= 0)
                {
                    titlefire.RemoveAt(i);
                }
            }

            if (fireCounter < 0)
            {
                fireRandomizer = (PlayerState.Random.Next(0, 10001) - 5000) / 10f;
                if (GameState.State == GameStates.GameStart)
                {
                    fireCounter = PlayerState.Random.Next(0, 60); 
                }
                else if (GameState.State == GameStates.TitleScreen)
                {
                    fireCounter = PlayerState.Random.Next(0, 80); 
                }
                else if (GameState.State == GameStates.GameEnd)
                {
                    fireCounter = PlayerState.Random.Next(0, 80); 
                }
            }

            fireCounter--;
            flameDirection += 0.01f * fireRandomizer;
            flameDirection *= 0.88f;
        }

        public void Draw(SpriteBatch SpriteBatch)
        {
            if (GameState.State == GameStates.GameStart)
            {
                int k = fire.Count - 1;
                while (k >= 0)
                {
                    fire[k].Draw(SpriteBatch);
                    k--;
                } 
            }
            else if (GameState.State == GameStates.TitleScreen || GameState.State == GameStates.GameEnd)
            {
                int l = titlefire.Count - 1;
                while (l >= 0)
                {
                    titlefire[l].Draw(SpriteBatch);
                    l--;
                } 
            }
        }

        public void titleDraw(SpriteBatch SpriteBatch)
        {
            for (int i = fire.Count - 1; i >= 0; i--)
            {
                fire[i].Draw(SpriteBatch);
            }
        }

        private void Add(Vector2 LightPosition)
        {
            direction = LightPosition - prevLightPosition;
            for (int j = fireExtender.Count - 1; j >= 0; j--)
            {
                for (float i = 0; i < 2; i++)
                {
                    fire.Add(new Spark(sparkTexture, fireExtender[j] + LightPosition + ((2.0f / i) * direction), flameDirection + (1.0f / i) * direction.Length(), direction));
                }
            }
            prevLightPosition = LightPosition;
        }

        private void TitleAdd(Vector2 TorchPosition, float scale)
        {
            if (GameState.State == GameStates.TitleScreen)
            {
                for (int j = titleScreenFireExtender.Count - 1; j >= 0; j--)
                {
                        for (float i = 0; i < 4; i++)
                        {
                            titlefire.Add(new TitleSpark(sparkTexture, scale * titleScreenFireExtender[j] + TorchPosition, flameDirection, Vector2.Zero, scale));
                        }
                }
            }
            else if (GameState.State == GameStates.GameEnd)
            {
                for (int j = endScreenFireExtender.Count - 1; j >= 0; j--)
                {
                    for (float i = 0; i < 1; i++)
                    {
                        titlefire.Add(new TitleSpark(sparkTexture, scale * endScreenFireExtender[j] * new Vector2(1.33f, 1.0f)+ TorchPosition, flameDirection, Vector2.Zero, scale));
                    }
                }
            }
        }
    }
}
