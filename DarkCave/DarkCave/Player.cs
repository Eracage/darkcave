﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace DarkCave
{
	class Player
	{
        private SpriteBatch spriteBatch;
        private HitboxController hitboxController;

	    private const int hitboxWidth = 30;
	    private const int hitboxHeight = 80;
	    private const float jumpPower = 10f;
	    private const float speed = 5;
	    private const float flyPower = 0.5f;
	    private const float gravity = 0.8f;
	    private readonly Vector2 spawnpoint;
	    private const int cameraDeadzone = DarkCave.Width/8;
        private const float lightDistance = 17.5f;
        private const float lightHeight = 68f;
        private const float crouchLightHeight = 44f;
	    private float lightX;
	    private float lightY;
	    private Vector2 lightPosition;
	    private Vector2 lightDirection;

	    public Vector2 LightPosition 
        {get{return lightPosition;} }

	    public Vector2 Position;
        public Hitbox PlayerHitbox;
        public Vector2 Velocity = Vector2.Zero;
	    private readonly PlayerAnimator playerAnimator;

	    public HitboxController SetHitboxController
        { set { hitboxController = value; } }

		public Player(Texture2D Texture, Vector2 Spawnpoint)
		{
			playerAnimator = new PlayerAnimator(Texture);
			Position = Spawnpoint;
            spawnpoint = Spawnpoint;
		    lightX = lightDistance;
		    lightY = lightHeight;
            PlayerHitbox = new Hitbox(Position, hitboxWidth, hitboxHeight, 0, new Vector2(hitboxWidth / 2, hitboxHeight));
		}

		public void Update(GameTime gameTime)
        {

			Position += Velocity;

            PlayerHitbox.Update(Position);

            hitboxController.Update();

            playerAnimator.Animator.animate(gameTime);

            if (Position.X + Camera.Position.X + Velocity.X > DarkCave.Width / 2 + cameraDeadzone)
            {
                Camera.Position.X = -(int)Position.X + DarkCave.Width / 2 + cameraDeadzone;
            }
            else if (Position.X + Camera.Position.X + Velocity.X < DarkCave.Width / 2 - cameraDeadzone)
            {
                Camera.Position.X = -(int)Position.X + DarkCave.Width / 2 - cameraDeadzone;
            }

            updateLightPosition();

            Velocity.X = 0;

            if (PlayerState.Airborne)
            {
                Velocity.Y += gravity;
            }
            if (Position.Y > DarkCave.Height + playerAnimator.Animator.FramesizeY)
            {
                PlayerState.Dead = true;
            }
		}

		public void Draw(SpriteBatch SpriteBatch, Vector2 Camera)
		{
			spriteBatch = SpriteBatch;

			spriteBatch.Draw(
				playerAnimator.Animator.Texture,
				new Vector2((int)Position.X, (int)Position.Y) + Camera,
				new Rectangle(playerAnimator.Animator.CurrentFrameX, playerAnimator.Animator.CurrentFrameY,
				              playerAnimator.Animator.FramesizeX, playerAnimator.Animator.FramesizeY),
				Color.White,
				0f,
				new Vector2((float)playerAnimator.Animator.FramesizeX/2,
				            playerAnimator.Animator.FramesizeY),
				1,
				playerAnimator.Animator.FlipIt,
				0.8f);
		}

		public void Respawn()
		{
			Position = spawnpoint;
            playerAnimator.Reset();
            PlayerState.Dead = false;
			Velocity = Vector2.Zero;
			Camera.Position = Vector2.Zero;
            updateLightPosition();
		}

		public void Stand()
		{
			playerAnimator.Stand();
		}

		public void Move(int direction)
		{

            if (direction < 0)
            {
                playerAnimator.Animator.Flip = true;
                lightX = -lightDistance;
            }
            else if (direction > 0)
            {
                playerAnimator.Animator.Flip = false;
                lightX = lightDistance;
            }

		    if (!PlayerState.Crouched)
	        {
		        Velocity.X = direction*speed;
	            playerAnimator.Move();
	        }
			else
            {
                if (PlayerState.Airborne)
                    Velocity.X = direction * speed / 3f;
                else
                    Velocity.X = direction *speed / 3f;

                playerAnimator.MoveCrouched();
			}

		}

		public void Jump()
		{
			playerAnimator.Jump();
            if (!PlayerState.Airborne)
			{
				Velocity.Y = -jumpPower;

				//irrotetaan pelaaja tilestä
				Position.Y -= 2;

			}
			else if (Velocity.Y < 0)
			{
				Velocity.Y -= flyPower;
			}
		}

		public void Attack()
		{
			playerAnimator.Attack();
		}

		public void Crouch()
		{
		    if (!PlayerState.MustCrouch)
		    {
		        playerAnimator.Crouch();
		    }
		}

        public void MustStayCrouched()
        {
            //playerAnimator.Staycrouched();
            PlayerState.Crouched = true;
            PlayerState.MustCrouch = true;
        }

	    private void updateLightPosition ()
	    {
            if (playerAnimator.Animator.CurrentFrame < 11 || playerAnimator.Animator.CurrentFrame > 29)
		    {
                lightY = lightHeight;
		    }
            else if (playerAnimator.Animator.CurrentFrame == 11)
            {
                lightY = lightHeight - 0.3f * (lightHeight - crouchLightHeight);
            }
            else if (playerAnimator.Animator.CurrentFrame == 12)
            {
                lightY = lightHeight - 0.5f * (lightHeight - crouchLightHeight);
            }
            else if (playerAnimator.Animator.CurrentFrame < 23)
            {
                lightY = crouchLightHeight;
            }

            lightDirection = (new Vector2(Position.X + lightX - 1.5f, Position.Y - lightY) - lightPosition);
            if (lightDirection.Length() < 300)
            {
                lightPosition += 0.3f * lightDirection; 
            }
            else
            {
                lightPosition += lightDirection;
            }
	    }
	}
}