﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DarkCave
{
    class Animator
    {
        #region Main Values

        float timer = 0f;
        int currentFrame;
        float fps;
        int firstFrame;
        int frames;
        int framesizeX;
        int framesizeY;
        bool flip = false;
        bool transition = false;
        bool backwards = false;
        int memFirstFrame;
        int memFrames;
        int memCurrentFrame;
        float memFps;
        int counter;

        //Vector2 position;
        Texture2D texture;

        public Animator(Texture2D Texture, int Frames, int FramesizeX, int FramesizeY, float Fps = 5, int FirstFrame = 0, bool Flip = false)
        {
            texture = Texture;
            frames = Frames;
            framesizeX = FramesizeX;
            framesizeY = FramesizeY;
            fps = Fps;
            firstFrame = FirstFrame;
            flip = Flip;
            backwards = false;
            timer = 0f;
        }

        #endregion

        #region Functions

        public void animate(GameTime gameTime)
        {
            if (!backwards)
            {
                timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                if (timer > 1000 / fps)
                {
                    currentFrame++;
                    if (currentFrame >= firstFrame + frames)
                    {
                        currentFrame = firstFrame;
                    }
                    timer -= 1000 / fps;
                    counter++;
                }
            }
            else
            {
                timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                if (timer > 1000 / fps)
                {
                    currentFrame--;
                    if (currentFrame < firstFrame)
                    {
                        currentFrame = firstFrame + frames -1;
                    }
                    timer -= 1000 / fps;
                    counter++;
                }
            }

            if (transition)
            {
                if (!(counter < frames))
                {
                    firstFrame = memFirstFrame;
                    frames = memFrames;
                    currentFrame = memCurrentFrame;
                    fps = memFps;
                    transition = false;
                    backwards = false;
                }
            }
        }

        public void ChangeAnimation(int FirstFrameofLoop, int FramesinLoop, int AnimationStartPoint, float Fps)
        {
            firstFrame = FirstFrameofLoop;
            frames = FramesinLoop;
            currentFrame = AnimationStartPoint;
            fps = Fps;
            memFirstFrame = firstFrame;
            memFrames = frames;
            memCurrentFrame = currentFrame;
            memFps = fps;
            timer = 0;
            backwards = false;
        }

        public void AnimationTransition(int FirstFrameofTransition, int FramesinTransition, float TransitionFps, bool TransitionBackwards = false)
        {
            if (!transition)
            {
                memFirstFrame = firstFrame;
                memFrames = frames;
                memCurrentFrame = currentFrame;
                memFps = fps;
                transition = true;
            }
            firstFrame = FirstFrameofTransition;
            frames = FramesinTransition;
            fps = TransitionFps;
            backwards = TransitionBackwards;
            counter = 0;
            timer = 0;

            if (!backwards)
            {
                currentFrame = firstFrame;
            }
            else
            {
                currentFrame = firstFrame + frames - 1;
            }
        }

        public SpriteEffects FlipIt
        {
            get 
            {
                if (!flip)
                {
                    return SpriteEffects.None;
                }
                else
                {
                    return SpriteEffects.FlipHorizontally;
                }
            }
        }

        public bool Flip
        {
            get { return flip; }
            set { flip = value; }
        }

        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

        public int FirstFrame
        {
            get { return firstFrame; }
            set { firstFrame = value; }
        }

        public int Frames
        {
            get { return frames; }
            set { frames = value; }
        }

        public int FramesizeX
        {
            get { return framesizeX; }
            set
            {
                if (FramesizeX != 0)
                {
                    framesizeX = value;
                }
                else
                {
                    framesizeX = 1;
                }
            }
        }

        public int FramesizeY
        {
            get { return framesizeY; }
            set { framesizeY = value; }
        }

        public int CurrentFrame
        {
            get { return currentFrame; }
            set { currentFrame = value; }
        }

        public int CurrentFrameX
        {
            get { return ((currentFrame % (texture.Width / framesizeX)) * framesizeX); }
        }

        public int CurrentFrameY
        {
            get { return ((int)(Math.Floor((double)currentFrame / ((double)texture.Width / (double)framesizeX))) * framesizeY); }
        }

        #endregion

    }
}
