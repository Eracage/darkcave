﻿using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace DarkCave
{
    class PlayerAnimator
    {
        bool stand = false;
        bool move = false;
        bool moveCrouched = false;
        bool crouch = false;
        bool jumped = true;
        bool attack = false;

        public Animator Animator;

        public PlayerAnimator(Texture2D Texture)
        {

            Animator = new Animator(Texture, 1, 40, 80, 1);
        }

        public void Reset()
        {
            stand = false;
            move = false;
            moveCrouched = false;
            crouch = false;
            jumped = true;
            attack = false;
        }
        
        public void Stand()
        {

            if (!stand && !PlayerState.Airborne)
            {
                Animator.ChangeAnimation(0, 1, 0, 2);

                if (move)
                {
                    Debug.WriteLine("Pysähtyy liikkeestä");
                    Animator.AnimationTransition(11, 1, 4);
                }
                else if (moveCrouched)
                {
                    Debug.WriteLine("Pysähtyy kyykystä seisomaan");
                    Animator.AnimationTransition(11, 3, 8, true);
                }
                else if (crouch)
                {
                    Debug.WriteLine("Nousee kyykystä seisomaan");

                    Animator.AnimationTransition(11, 3, 8, true);
                }
                else if (jumped)
                {
                    Debug.WriteLine("Laskeutuu hypystä");

                    Animator.AnimationTransition(11, 3, 8, true);
                }
                else if (attack)
                {
                }
                else
                {

                    Debug.WriteLine("Stand");
                }

                stand = true;
                move = false;
                moveCrouched = false;
                crouch = false;
                jumped = false;
                attack = false;
            }
        }

        public void Move()
        {
            if (!move && !PlayerState.Airborne)
            {
                Animator.ChangeAnimation(1, 10, 1, 10);

                if (stand)
                {
                    Debug.WriteLine("Lähtee seisomasta liikkeelle");
                }
                else if (moveCrouched)
                {
                    Debug.WriteLine("Nousee liikkeessä kyykystä seisomaan");
                    Animator.AnimationTransition(11, 3, 16, true);
                }
                else if (crouch)
                {
                    Debug.WriteLine("Lähtee kyykyssä liikkeelle");
                    Animator.AnimationTransition(11, 3, 16, true);
                }
                else if (jumped)
                {
                    Animator.AnimationTransition(10, 3, 8, true);
                }
                else if (attack)
                {
                }
                else
                {
                    Debug.WriteLine("Move");
                }

                stand = false;
                move = true;
                moveCrouched = false;
                crouch = false;
                jumped = false;
                attack = false;
            }
        }

        public void MoveCrouched()
        {
            if (!moveCrouched && !PlayerState.Airborne)
            {
                Animator.ChangeAnimation(15, 8, 15, 5f);

                if (stand)
                {
                    Debug.WriteLine("Lähtee seisomasta kyykyssä liikkeelle");
                    Animator.AnimationTransition(11, 3, 8);
                }
                else if (move)
                {
                    Debug.WriteLine("Menee liikkeessä kyykkyyn");
                    Animator.AnimationTransition(11, 3, 8);
                }
                else if (crouch)
                {
                    Debug.WriteLine("Lähtee kyykyssä liikkeelle");
                }
                else if (jumped)
                {
                    Animator.AnimationTransition(12, 1, 16);
                }
                else if (attack)
                {
                }
                else
                {
                    Debug.WriteLine("MoveCrouched");
                }

                stand = false;
                move = false;
                moveCrouched = true;
                crouch = false;
                jumped = false;
                attack = false;
            }
        }

        public void Crouch()
        {

            if (!crouch && !PlayerState.Airborne)
            {
                Animator.ChangeAnimation(13, 2, 13, 2);

                if (stand)
                {
                    Debug.WriteLine("Menee kyykkyyn seisomasta");
                    Animator.AnimationTransition(11, 2, 8);
                }
                else if (move)
                {
                    Debug.WriteLine("Menee kyykkyyn liikkeessä");
                }
                else if (moveCrouched)
                {
                    Debug.WriteLine("Pysähtyy kyykyssä");
                }
                else if (jumped)
                {
                    Animator.AnimationTransition(12, 1, 16);
                }
                else if (attack)
                {
                }
                else
                {
                    Debug.WriteLine("Crouch");
                }

                stand = false;
                move = false;
                moveCrouched = false;
                crouch = true;
                jumped = false;
                attack = false;
            }
        }

        public void Jump()
        {

            if (!jumped && !PlayerState.Airborne)
            {
                Animator.ChangeAnimation(45, 1, 45, 1);

                if (stand)
                {
                }
                else if (move)
                {
                }
                else if (moveCrouched)
                {
                }
                else if (crouch)
                {
                }
                else if (attack)
                {
                }
                else
                {
                    Debug.WriteLine("Jump");
                }

                stand = false;
                move = false;
                moveCrouched = false;
                crouch = false;
                jumped = true;
                attack = false;
            }
        }

        public void Attack()
        {
            if (!jumped && !crouch && !PlayerState.Airborne)
            {
                Animator.AnimationTransition(31, 3, 15);

                if (stand)
                {
                }
                else if (move)
                {
                }
                else if (moveCrouched)
                {
                }
                else if (crouch)
                {
                }
                else if (jumped)
                {
                }
                else
                {
                    Debug.WriteLine("Hyökkää");
                }

                //stand = false;
                //move = false;
                //moveCrouched = false;
                //crouch = false;
                ////jump = false;
                attack = true;
            }
        }

        public void Staycrouched()
        {
            Animator.ChangeAnimation(13, 2, 13, 2);
            Animator.AnimationTransition(13, 2, 13);
        }
    }
}
