﻿using System;
using Microsoft.Xna.Framework;

namespace DarkCave
{
    class Hitbox
    {
        Vector2 origin;
        Rectangle box;
        int type;
        public Hitbox(Vector2 Position, int HitboxWidth, int HitboxHeight, int Type, Vector2 Origin)
        {
            type = Type;
            origin = Origin;
            box = new Rectangle((int)Position.X - (int)origin.X, (int)Position.Y - (int)origin.Y, HitboxWidth, HitboxHeight);
        }

        public void Update(Vector2 Position)
        {
            box.X = (int)Position.X - (int)origin.X;
            box.Y = (int)Position.Y - (int)origin.Y;
        }

        public Rectangle Box
        {
            get { return box; }
        }

        public Vector2 Position
        {
            get { return new Vector2(box.X, box.Y); }
        }

        public int Width
        {
            get { return box.Width; }
            set 
            {
                if (box.Width != 0)
                {
                    origin = new Vector2((float)(Math.Round((double)value * ((double)origin.X / (double)box.Width))), origin.Y);
                }
                else
                {
                    origin = new Vector2(0, origin.Y);
                }
                box.Width = value;
            }
        }

        public int Height
        {
            get { return box.Height; }
            set 
            {
                if (box.Height != 0)
                {
                    origin = new Vector2(origin.X,(float)(Math.Round((double)value * ((double)origin.Y / (double)box.Height))));
                }
                else
                {
                    origin = new Vector2(origin.X, 0);
                }
                box.Height = value;
            }
        }

        public int Type
        {
            get { return type; }
            set { type = value; }
        }

    }
}
