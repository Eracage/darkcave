﻿namespace DarkCave
{
    public enum GameStates
    {
        TitleScreen = 0,
        GameStart,
        GameEnd,
		LevelEnd,
		LevelChange,
    }

    public static class GameState
    {
        public static GameStates State = GameStates.TitleScreen;
    }
}
