using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;


namespace DarkCave
{
    public class DarkCave : Microsoft.Xna.Framework.Game
    {
        #region Main values

        public const int Width = 1280;
        public const int Height = 768;
        private int timer;

        private readonly GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private KeyboardState keyboardState;
        private Map map;
        public static int CurrentMap = 1;
        private Song background;

        private Player player;
        private readonly Controller controller = new Controller();
        private HitboxController hitboxController;
        private ParticleController particleController;

        private Texture2D titleScreen;
        private Texture2D endScreen;
        private Texture2D levelChangeScreen;

        private Texture2D musta;
        private Texture2D tilesheet;
        private Texture2D deadscreen;

        public DarkCave()
        {
            Window.Title = "Dark Cave";
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = Width;
            graphics.PreferredBackBufferHeight = Height;
            graphics.ApplyChanges();
            Content.RootDirectory = "Content";
        }

        #endregion

        #region Initialize

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        #endregion

        #region LoadContent

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            map = new Map("Content/Maps/1.txt");

            titleScreen = Content.Load<Texture2D>("Grafiikat/titlescreen");

            endScreen = Content.Load<Texture2D>("Grafiikat/endscreen");

            tilesheet = Content.Load<Texture2D>("Tiles/caveset1");

            background = Content.Load<Song>("Background");

            MediaPlayer.IsRepeating = true;

            MediaPlayer.Volume = 0.1f;

            MediaPlayer.Play(background);

            Tile.setSpritesheet(tilesheet, 64);

            map.CreateTypeDefinations();

            player = new Player(Content.Load<Texture2D>("Grafiikat/playersprite"), new Vector2(600, 100));

            hitboxController = new HitboxController(player, map.Tiles);

            player.SetHitboxController = hitboxController;

            particleController = new ParticleController(Content.Load<Texture2D>("Grafiikat/spark"));

            musta = Content.Load<Texture2D>("Grafiikat/soihtuvalo");

            levelChangeScreen = Content.Load<Texture2D>("Grafiikat/levelchange");

            deadscreen = Content.Load<Texture2D>("Grafiikat/udead");
        }

        #endregion

        #region UnloadContent

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        #endregion

        #region Update

        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            keyboardState = Keyboard.GetState();
            controller.ReadInput(keyboardState, player, gameTime);

            switch (GameState.State)
            {
                case GameStates.GameStart:
                    if (!PlayerState.Dead)
                    {
                        timer += 1;

                        player.Update(gameTime);
                    }
                    break;
                case GameStates.LevelChange:
                    map = new Map("Content/Maps/" + CurrentMap + ".txt");
                    map.CreateTypeDefinations();
                    hitboxController.MapTiles = map.Tiles;
                    player.Respawn();
                    GameState.State = GameStates.GameStart;
                    break;
            }

            if (GameState.State == GameStates.LevelEnd)
            {
                if (CurrentMap > 3) // tai mik� se viimenen mappi ikin� tulee olemaan
                {
                    GameState.State = GameStates.GameEnd;
                }
            }

            particleController.Update(player.LightPosition);
            
            base.Update(gameTime);
        }

        #endregion

        #region Draw

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(new Color(46, 27, 20));

            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);

            if (GameState.State == GameStates.GameStart || GameState.State == GameStates.LevelEnd)
            {
                //, SamplerState.LinearClamp, DepthStencilState.DepthRead, RasterizerState.CullCounterClockwise);


                for (int i = 0; i < map.Tiles.Count(); i++)
                {
                    map.Tiles[i].Draw(spriteBatch, Camera.Position);
                }

                player.Draw(spriteBatch, Camera.Position);

                particleController.Draw(spriteBatch);

                if (PlayerState.Dead)
                {
                    spriteBatch.Draw(deadscreen, new Vector2(Width/2, Height/2), null, Color.White, 0f,
                                     new Vector2(deadscreen.Width/2f, deadscreen.Height/2f), 1f, SpriteEffects.None, 1f);
                }

                if (GameState.State == GameStates.LevelEnd)
                {
                    spriteBatch.Draw(levelChangeScreen, new Vector2(Width/2, Height/2), null, Color.White, 0f,
                                     new Vector2(levelChangeScreen.Width/2f, levelChangeScreen.Height/2f), 1f,
                                     SpriteEffects.None, 1f);
                }
            }

            if (Camera.BlackEnabled)
            {
                spriteBatch.Draw(musta, player.LightPosition + Camera.Position, null, Color.White, timer / 3 * 3,
                                 new Vector2(musta.Width / 2f, musta.Height / 2f), 1.2f + (float)(Math.Sin(timer / 25f)) / 6f,
                                 SpriteEffects.None, 0.9f);
            }

            spriteBatch.End();

            switch (GameState.State)
            {
                case GameStates.TitleScreen:
                    GraphicsDevice.Clear(Color.Black);
                    spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);
                    spriteBatch.Draw(titleScreen, new Vector2(0, 0), Color.White);
                    particleController.Draw(spriteBatch);
                    spriteBatch.End();
                    break;
                case GameStates.GameEnd:
                    GraphicsDevice.Clear(Color.Black);
                    spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.Additive);
                    spriteBatch.Draw(endScreen, new Vector2(0, 0), Color.White);
                    particleController.Draw(spriteBatch);
                    spriteBatch.End();
                    break;
            }

            base.Draw(gameTime);
        }

        #endregion
    }
}