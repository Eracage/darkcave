﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Graphics;
//using Microsoft.Xna.Framework.Input;
//using System.Diagnostics;

//namespace DarkCave
//{
//    class AnimatedSprite
//    {
//        #region Public and Private values
//        Texture2D spriteTexture;
//        float timer = 0f;
//        float animointinopeus = 200f;
//        int currentFrame = 0;
//        int spriteWidth = 32;
//        int spriteHeight = 48;
//        int spriteSpeed = 2;
//        Rectangle sourceRect;
//        SpriteEffects suunta = SpriteEffects.None;
//        Vector2 position;
//        Vector2 origin;


//        public Vector2 Position
//        {
//            get { return position; }
//            set { position = value; }
//        }

//        public SpriteEffects Suunta
//        {
//            get { return suunta; }
//            set { suunta = value; }
//        }

//        public Vector2 Origin
//        {
//            get { return origin; }
//            set { origin = value; }
//        }

//        public Texture2D Texture
//        {
//            get { return spriteTexture; }
//            set { spriteTexture = value; }
//        }

//        public Rectangle SourceRect
//        {
//            get { return sourceRect; }
//            set { sourceRect = value; }
//        }

//        public AnimatedSprite(Texture2D texture, int currentFrame, int spriteWidth, int spriteHeight)
//        {
//            this.spriteTexture = texture;
//            this.currentFrame = currentFrame;
//            this.spriteWidth = spriteWidth;
//            this.spriteHeight = spriteHeight;
//        }

//        KeyboardState currentKBState;
//        KeyboardState previousKBState;
//        #endregion

//        #region HandleSpriteMovement
//        public void HandleSpriteMovement(GameTime gameTime)
//        {
//            previousKBState = currentKBState;
//            currentKBState = Keyboard.GetState();

//            sourceRect = new Rectangle(currentFrame * spriteWidth, 0, spriteWidth, spriteHeight);

//            if (currentKBState.GetPressedKeys().Length == 0)
//            {
//                if (currentFrame >= 0 && currentFrame < 7)
//                {
//                    currentFrame = 16;
//                }
//            }
//            spriteSpeed = 3;
//            animointinopeus = 90;


//            if (currentKBState.IsKeyDown(Keys.Right) == true)
//            {

//                if (currentKBState.IsKeyDown(Keys.Left) == false)
//                {
//                    suunta = SpriteEffects.None;
//                }

//                AnimateRight(gameTime);
//                if (position.X < 780)
//                {
//                    position.X += spriteSpeed;
//                }
//            }

//            if (currentKBState.IsKeyDown(Keys.Left) == true)
//            {
//                //AnimateLeft(gameTime);
//                if (currentKBState.IsKeyDown(Keys.Right) == false)
//                {
//                    suunta = SpriteEffects.FlipHorizontally;
//                }

//                AnimateRight(gameTime);
//                if (position.X > 20)
//                {
//                    position.X -= spriteSpeed;
//                }
//            }

//            if ((currentKBState.IsKeyDown(Keys.Right) == true) && (currentKBState.IsKeyDown(Keys.Left) == true))
//            {
//                currentFrame = 2;
//                timer = 0f;
//                currentFrame = 16;

//            }
//            if ((currentKBState.IsKeyDown(Keys.Right) == false) && (currentKBState.IsKeyDown(Keys.Left) == false))
//            {
//                currentFrame = 2;
//                timer = 0f;
//                currentFrame = 16;
//            }


//            origin = new Vector2(sourceRect.Width / 2, sourceRect.Height / 2);
//        }
//        #endregion

//        #region Voids
//        public void AnimateRight(GameTime gameTime)
//        {

//            timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

//            if (timer > animointinopeus)
//            {
//                currentFrame++;

//                if (currentFrame > 7)
//                {

//                    currentFrame = 0;
//                }
//                timer = 0f;
//            }
//        }

//        #endregion

//    }
//}
