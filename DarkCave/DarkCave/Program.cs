using System;

namespace DarkCave
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (DarkCave game = new DarkCave())
            {
                game.Run();
            }
        }
    }
#endif
}

